//
//  CustomCell.swift
//  GitCoverTest
//
//  Created by Celso Andre on 22/10/19.
//  Copyright © 2019 Celso Andre. All rights reserved.
//

import UIKit

class CustomCell: UITableViewCell {
    var resposityName: String?
    var resposityImagePath: String?
    var resposityStars: Int?

    private var resposityNameView: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.textColor = .black
        label.textAlignment = .left
        label.numberOfLines = 1
        label.lineBreakMode = .byTruncatingTail
        label.adjustsFontSizeToFitWidth = false
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private var resposityImageView: UIImageView = {
        var imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()

    private var resposityStarsView: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.textColor = .black
        label.textAlignment = .left
        label.numberOfLines = 1
        label.lineBreakMode = .byTruncatingTail
        label.adjustsFontSizeToFitWidth = false
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        self.addSubViews()
        self.configureViews()

    }

    // MARK: - Functions
    override func layoutSubviews() {
        super.layoutSubviews()

        if let image = resposityImagePath {
            let url = URL(string: image)
            resposityImageView.kf.setImage(with: url)
        }

        if let name = resposityName {
            resposityNameView.text = name
        }

        if let stars = resposityStars {
            resposityStarsView.text = "Estrelas: \(stars)"
        }
    }

    func addSubViews() {
        self.addSubview(resposityImageView)
        self.addSubview(resposityNameView)
        self.addSubview(resposityStarsView)
    }

    func configureViews() {
        self.resposityImageView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        self.resposityImageView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        self.resposityImageView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        self.resposityImageView.widthAnchor.constraint(equalToConstant: 100).isActive = true
        self.resposityImageView.heightAnchor.constraint(equalToConstant: 100).isActive = true

        self.resposityNameView.leftAnchor.constraint(equalTo: resposityImageView.rightAnchor, constant: 8).isActive = true
        self.resposityNameView.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        self.resposityNameView.topAnchor.constraint(equalTo: self.topAnchor, constant: 16).isActive = true

        self.resposityStarsView.leftAnchor.constraint(equalTo: resposityImageView.rightAnchor, constant: 8).isActive = true
        self.resposityStarsView.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        self.resposityStarsView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -16).isActive = true
        self.resposityStarsView.topAnchor.constraint(equalTo: resposityNameView.bottomAnchor).isActive = true

    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
