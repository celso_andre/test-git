//
//  LoadingCell.swift
//  GitCoverTest
//
//  Created by Celso Andre on 23/10/19.
//  Copyright © 2019 Celso Andre. All rights reserved. UITableViewCell
//

import UIKit

class LoadingCell: UITableViewCell {

    var startAnimated = false

    var activityIndicatorView: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.color = .black
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        return activityIndicator
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
           super.init(style: style, reuseIdentifier: reuseIdentifier)

        self.addSubViews()
        self.configureViews()
   }

    // MARK: - Functions
    func addSubViews() {
         self.addSubview(activityIndicatorView)
    }

    func configureViews() {
        activityIndicatorView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        activityIndicatorView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        activityIndicatorView.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 15).isActive = true
        activityIndicatorView.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 15).isActive = true
    }
   override func layoutSubviews() {
       super.layoutSubviews()

        if !startAnimated {
            activityIndicatorView.startAnimating()
        } else {
            activityIndicatorView.stopAnimating()
        }

   }

   required init?(coder: NSCoder) {
       fatalError("init(coder:) has not been implemented")
   }
}
