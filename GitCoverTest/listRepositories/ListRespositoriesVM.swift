//
//  ListRespositoriesVM.swift
//  GitCoverTest
//
//  Created by Celso Andre on 22/10/19.
//  Copyright © 2019 Celso Andre. All rights reserved.
//

import Foundation


class ListRespositoriesVM {
    var apiService: GitApiService?
    var repositories: [Item]?

    init() {
        apiService = GitApiService()
    }

    func fetchRespositories(completation: @escaping (Bool) -> ()){

        guard let apiService = apiService else { return completation(false) }

        apiService.fetchMovies { itens in
            print("quantidade de itens: \(itens.count)")
            self.repositories = itens
            completation(true)
        }
    }

    func numbersOfItemsInSection(section: Int) -> Int {
        return repositories?.count ?? 0
    }

    func titleForItemAtIndexPath(indexPath: IndexPath) -> Item? {

        guard let repositories = repositories else {return nil}

        let repository = repositories[indexPath.row]

        return repository
    }

}
