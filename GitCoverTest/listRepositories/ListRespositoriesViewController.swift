//
//  ListRespositoriesViewController.swift
//  GitCoverTest
//
//  Created by Celso Andre on 22/10/19.
//  Copyright © 2019 Celso Andre. All rights reserved.
//

import UIKit
import Kingfisher

class ListRespositoriesViewController: UITableViewController {

    // MARK: - Variables
    private let heightRow: CGFloat = 120.0
    var viewModel = ListRespositoriesVM()
    var fetchingMore = false

    // MARK: - Outlets

    override func viewDidLoad() {
        super.viewDidLoad()

        self.configTableView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        fetchData()
    }

    // MARK: - Functions
    func configTableView() {

       self.tableView.register(CustomCell.self, forCellReuseIdentifier: "gitCell")
       self.tableView.register(LoadingCell.self, forCellReuseIdentifier: "loadingCell")
       self.tableView.rowHeight = UITableView.automaticDimension
       self.tableView.estimatedRowHeight = 60.0
   }

    func fetchData() {
        viewModel.fetchRespositories { (result) in
            if result {
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
    }

    func configureTableViewCell(_ cell: CustomCell, indexPath: IndexPath){

        let repository = viewModel.titleForItemAtIndexPath(indexPath: indexPath)

        cell.resposityName = repository?.name ?? ""
        cell.resposityStars = repository?.stargazersCount
        cell.resposityImagePath = repository?.owner?.avatarURL
        cell.layoutSubviews()
    }

    func beginBatchFetch() {
        print("beginBatchFetch")
        fetchingMore = true
        tableView.reloadSections(IndexSet(integer: 1), with: .none)

        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
            if let newItens = self.viewModel.repositories {
                self.viewModel.repositories?.append(contentsOf: newItens)
                self.tableView.reloadData()
            }
            self.fetchingMore = false
        }
    }

    // MARK: - Actions
    @IBAction func refresh(_ sender: UIRefreshControl) {
        fetchData()
        sender.endRefreshing()
    
    }
    
}

// MARK: - Extension referring to UITableView functions
extension ListRespositoriesViewController {

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return viewModel.numbersOfItemsInSection(section: section)
        } else if section == 1 && fetchingMore {
            return 1
        }
        return 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "gitCell") as! CustomCell
            configureTableViewCell(cell, indexPath: indexPath)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "loadingCell") as! LoadingCell
            cell.activityIndicatorView.startAnimating()
            return cell
        }
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.heightRow
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let repository = viewModel.titleForItemAtIndexPath(indexPath: indexPath)
        print(repository?.fullName)
    }

    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height

        if offsetY > contentHeight - scrollView.frame.height {
            if !fetchingMore {
                beginBatchFetch()
            }
        }
    }
}

