//
//  GitApiService.swift
//  GitCoverTest
//
//  Created by Celso Andre on 22/10/19.
//  Copyright © 2019 Celso Andre. All rights reserved.
//

import Foundation

class GitApiService: NSObject {
    private let basePath = "https://api.github.com/search/repositories?q=language:swift&sort=stars"

    private let configuration: URLSessionConfiguration = {
        let config = URLSessionConfiguration.default

        config.allowsCellularAccess = true

        config.httpAdditionalHeaders = ["Content-Type" : "applications/json"]

        config.timeoutIntervalForRequest = 30.0

        config.httpMaximumConnectionsPerHost = 5

        return config
    }()


    func fetchMovies(completion: @escaping ( _ itens: [Item]) -> ()){

        guard let url = URL(string: basePath) else { return }

        let session = URLSession(configuration: configuration)

        let dataTask = session.dataTask(with: url) { (data, response, error) in
            if error == nil {

                guard let response = response as? HTTPURLResponse else {return}

                if response.statusCode == 200 {
                    guard let data = data else {return}

                    do{
                        let obj =  try JSONDecoder().decode(GitResults.self, from: data)
                        let itens = obj.items
                        if itens.count > 0 {
                            completion(itens)
                        }

                    } catch {

                        print("\(error)")
                    }
                }

            } else {
                print(error?.localizedDescription)
            }
        }
        dataTask.resume()
    }

}

