//
//  EntityMock.swift
//  GitCoverTestTests
//
//  Created by Celso Andre on 23/10/19.
//  Copyright © 2019 Celso Andre. All rights reserved.
//

import Foundation
@testable import GitCoverTest

class EntityMock {

    static var licenseMock: License {
        let entity = License(key: .mit,
        name: .mitLicense,
        spdxID: .mit,
            url: "https://api.github.com/licenses/mit",
            nodeID: .mDc6TGljZW5ZZTA)

        return entity
    }

    static var ownerMock: Owner {
        let entity = Owner(login: "vsouza",
            id: 484656,
            nodeID: "MDQ6VXNlcjQ4NDY1Ng==",
            avatarURL: "https://avatars3.githubusercontent.com/u/49566725?s=460&v=4",
            gravatarID: "",
            url: "",
            htmlURL: "",
            followersURL: "",
            followingURL: "",
            gistsURL: "",
            starredURL: "",
            subscriptionsURL: "",
            organizationsURL: "",
            reposURL: "",
            eventsURL: "",
            receivedEventsURL: "",
            type: .user,
            siteAdmin: false)
        return entity
    }


    static var itemMock: Item  {
        let entity = Item(id: 21700699,
        nodeID: "MDEwOlJlcG9zaXRvcnkyMTcwMDY5OQ==",
        name: "awesome-ios",
        fullName: "vsouza/awesome-ios",
        itemPrivate: false,
        owner: ownerMock,
        htmlURL: "https://github.com/vsouza/awesome-ios",
        itemDescription: "A curated list of awesome iOS ecosystem, including Objective-C and Swift Projects ",
        fork: false,
        url: "https://api.github.com/repos/vsouza/awesome-ios",
        forksURL: "https://api.github.com/repos/vsouza/awesome-ios/forks",
        keysURL: "https://api.github.com/repos/vsouza/awesome-ios/keys{/key_id}",
        collaboratorsURL: "https://api.github.com/repos/vsouza/awesome-ios/collaborators{/collaborator}",
        teamsURL:  "https://api.github.com/repos/vsouza/awesome-ios/teams",
        hooksURL: "https://api.github.com/repos/vsouza/awesome-ios/hooks",
        issueEventsURL: "https://api.github.com/repos/vsouza/awesome-ios/issues/events{/number}",
        eventsURL: "https://api.github.com/repos/vsouza/awesome-ios/events",
        assigneesURL: "https://api.github.com/repos/vsouza/awesome-ios/assignees{/user}",
        branchesURL: "https://api.github.com/repos/vsouza/awesome-ios/branches{/branch}",
        tagsURL: "https://api.github.com/repos/vsouza/awesome-ios/tags",
        blobsURL: "https://api.github.com/repos/vsouza/awesome-ios/git/blobs{/sha}",
        gitTagsURL: "https://api.github.com/repos/vsouza/awesome-ios/git/tags{/sha}",
        gitRefsURL: "https://api.github.com/repos/vsouza/awesome-ios/git/refs{/sha}",
        treesURL: "https://api.github.com/repos/vsouza/awesome-ios/git/trees{/sha}",
        statusesURL: "https://api.github.com/repos/vsouza/awesome-ios/statuses/{sha}",
        languagesURL: "https://api.github.com/repos/vsouza/awesome-ios/languages",
        stargazersURL: "https://api.github.com/repos/vsouza/awesome-ios/stargazers",
        contributorsURL: "https://api.github.com/repos/vsouza/awesome-ios/contributors",
        subscribersURL: "https://api.github.com/repos/vsouza/awesome-ios/subscribers",
        subscriptionURL: "https://api.github.com/repos/vsouza/awesome-ios/subscription",
        commitsURL: "https://api.github.com/repos/vsouza/awesome-ios/commits{/sha}",
        gitCommitsURL: "https://api.github.com/repos/vsouza/awesome-ios/git/commits{/sha}",
        commentsURL: "https://api.github.com/repos/vsouza/awesome-ios/comments{/number}",
        issueCommentURL: "https://api.github.com/repos/vsouza/awesome-ios/issues/comments{/number}",
        contentsURL: "https://api.github.com/repos/vsouza/awesome-ios/contents/{+path}",
        compareURL: "https://api.github.com/repos/vsouza/awesome-ios/compare/{head}",
        mergesURL: "https://api.github.com/repos/vsouza/awesome-ios/merges",
        archiveURL: "https://api.github.com/repos/vsouza/awesome-ios/{archive_format}{/ref}",
        downloadsURL: "https://api.github.com/repos/vsouza/awesome-ios/downloads",
        issuesURL: "https://api.github.com/repos/vsouza/awesome-ios/issues{/number}",
        pullsURL: "https://api.github.com/repos/vsouza/awesome-ios/pulls{/number}",
        milestonesURL: "https://api.github.com/repos/vsouza/awesome-ios/milestones{/number}",
        notificationsURL: "https://api.github.com/repos/vsouza/awesome-ios/notifications{?since,all,participating}",
        labelsURL: "https://api.github.com/repos/vsouza/awesome-ios/labels{/name}",
        releasesURL: "https://api.github.com/repos/vsouza/awesome-ios/releases{/id}",
        deploymentsURL: "https://api.github.com/repos/vsouza/awesome-ios/deployments",
        createdAt: "2014-07-10T16:03:45Z",
        updatedAt: "2019-10-23T18:33:51Z",
        pushedAt: "2019-10-23T14:53:44Z",
        gitURL: "git://github.com/vsouza/awesome-ios.git",
        sshURL: "git@github.com:vsouza/awesome-ios.git",
        cloneURL: "https://github.com/vsouza/awesome-ios.git",
        svnURL: "https://github.com/vsouza/awesome-ios",
        homepage: "http://awesomeios.com",
        size: 11660,
        stargazersCount: 33274,
        watchersCount: 33274,
        language: .swift,
        hasIssues: true,
        hasProjects: false,
        hasDownloads: true,
        hasWiki: false,
        hasPages: false,
        forksCount: 5607,
        mirrorURL: .none,
        archived: false,
        disabled: false,
        openIssuesCount: 1,
        license: licenseMock,
        forks: 5607,
        openIssues: 1,
        watchers: 33274,
        defaultBranch: .master,
        score: 1)
        return entity
    }
}

