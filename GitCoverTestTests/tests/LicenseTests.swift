//
//  LicenseTests.swift
//  GitCoverTestTests
//
//  Created by Celso Andre on 24/10/19.
//  Copyright © 2019 Celso Andre. All rights reserved.
//

import Foundation
import Quick
import Nimble

@testable import GitCoverTest

class LicenseTests:  QuickSpec {

   override func spec() {

        describe("verificando a licenca") {

           context("testando principais informações do licenca") {

            let entity = EntityMock.licenseMock

               it("should not be nil") {
                    expect(entity.name).notTo(beNil())
                    expect(entity.key).notTo(beNil())
                }
            }
        }
    }
}

