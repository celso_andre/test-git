//
//  OwnerTests.swift
//  GitCoverTestTests
//
//  Created by Celso Andre on 24/10/19.
//  Copyright © 2019 Celso Andre. All rights reserved.
//

import Foundation
import Quick
import Nimble

@testable import GitCoverTest

class OwnerTests:  QuickSpec {

   override func spec() {

        describe("verificando owner") {

           context("testando principais informações do owner") {

            let entity = EntityMock.ownerMock

               it("should not be nil") {
                    expect(entity.avatarURL).notTo(beNil())
                    expect(entity.id).notTo(beNil())
                }
            }
        }
    }
}
