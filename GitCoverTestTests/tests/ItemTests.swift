//
//  ItemTests.swift
//  GitCoverTestTests
//
//  Created by Celso Andre on 23/10/19.
//  Copyright © 2019 Celso Andre. All rights reserved.
//

import Foundation
import Quick
import Nimble

@testable import GitCoverTest

class ItemTests: QuickSpec {


   override func spec() {

       describe("verificando item") {

           context("testando principais informações do item") {

            let entity = EntityMock.itemMock

               it("should not be nil") {
                    expect(entity.id).notTo(beNil())
                    expect(entity.name).notTo(beNil())
                    expect(entity.stargazersCount).notTo(beNil())
                    expect(entity.owner).notTo(beNil())
                    expect(entity.owner?.avatarURL).notTo(beNil())
                }

            }
        }

    describe("verificando owner") {

       context("testando principais informações do owner") {

        let entity = EntityMock.ownerMock

           it("should not be nil") {
                expect(entity.avatarURL).notTo(beNil())

            }

        }
    }


    }
}
